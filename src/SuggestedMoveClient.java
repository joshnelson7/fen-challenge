import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Map;

public class SuggestedMoveClient {
    private static final String URL_BASE = "https://syzygy-tables.info/api/v2?fen=";


    public String getSuggestedMove(String fen) {
        //URL encode the spaces in the fen string
        fen = fen.replaceAll(" ", "%20");
        ObjectMapper mapper = new ObjectMapper();
        String suggestedMove = null;
        try (CloseableHttpClient httpClient = HttpClients.createDefault()){
            URI uri = new URI(URL_BASE + fen);
            HttpGet httpGet = new HttpGet(uri);

            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
                    int status = httpResponse.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = httpResponse.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }
            };
            //This is the raw response.  Need to parse the JSON objects
            String responseJson = httpClient.execute(httpGet, responseHandler);
            Map<String, Object> parsedResponse = mapper.readValue(responseJson, Map.class);

            LinkedHashMap<String, Object> moveList = (LinkedHashMap<String, Object>) parsedResponse.get("moves");
            suggestedMove = moveList.keySet().iterator().next();
        } catch (Exception e){
            System.out.println("An error occurred while getting suggested move: ");
            System.out.println(e);
        }
        return suggestedMove;
    }
}
