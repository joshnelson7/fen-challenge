public class FENChallenge {
    private Board board;
    private SuggestedMoveClient suggestedMoveClient;
    private static final String USAGE_MESSAGE = "Usage: fenchallenge --task1 | --task2 | --stretch \"fen\"";

    public static void main(String[] args) {
        FENChallenge challenge = new FENChallenge();

        if (args.length != 2) {
            System.out.println(USAGE_MESSAGE);
            return;
        }

        String fen = args[1];

        switch (args[0]) {
            case "--task1":
                challenge.doTask1(fen);
                break;
            case "--task2":
                challenge.doTask2(fen);
                break;
            case "--stretch":
                challenge.doStretch(fen);
                break;
            default:
                System.out.println(USAGE_MESSAGE);
        }
    }

    public FENChallenge() {
        board = new Board();
        suggestedMoveClient = new SuggestedMoveClient();
    }

    public void doTask1(String fen) {
        board.setBoardState(fen);
        System.out.println(board);
    }

    public void doTask2(String fen) {
        board.setBoardState(fen);
        String suggestedMove = suggestedMoveClient.getSuggestedMove(fen);
        board.makeMove(suggestedMove);
        System.out.println("Suggested move: " + suggestedMove);
        System.out.println("Updated FEN: " + board.getFENForBoard());
    }

    public void doStretch(String fen) {
        board.setBoardState(fen);
        System.out.println("Current position: \n");
        System.out.println(board + "\n");
        String suggestedMove = suggestedMoveClient.getSuggestedMove(fen);
        board.makeMove(suggestedMove);
        System.out.println("Suggested move: " + suggestedMove);
        System.out.println("Updated FEN: " + board.getFENForBoard());
        System.out.println("Updated position: \n");
        System.out.println(board);
    }
}
