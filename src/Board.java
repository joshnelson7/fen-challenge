import org.w3c.dom.Notation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Board {
    private char[][] board;
    private String fenSuffix;
    private static final Map<String, Integer> NOTATION_TO_BOARD_INDEX;

    //Mapping notation characters to array indices makes moving pieces easier
    static {
        Map<String, Integer> notationToBoard = new HashMap<>();
        notationToBoard.put("a", 0);
        notationToBoard.put("b", 1);
        notationToBoard.put("c", 2);
        notationToBoard.put("d", 3);
        notationToBoard.put("e", 4);
        notationToBoard.put("f", 5);
        notationToBoard.put("g", 6);
        notationToBoard.put("h", 7);
        notationToBoard.put("8", 0);
        notationToBoard.put("7", 1);
        notationToBoard.put("6", 2);
        notationToBoard.put("5", 3);
        notationToBoard.put("4", 4);
        notationToBoard.put("3", 5);
        notationToBoard.put("2", 6);
        notationToBoard.put("1", 7);
        NOTATION_TO_BOARD_INDEX = Collections.unmodifiableMap(notationToBoard);
    }

    public Board() {
    }

    public void setBoardState(String fen) {
        board = new char[8][8];

        int firstSpaceIndex = fen.indexOf(" ");
        String boardPart = fen.substring(0, firstSpaceIndex);
        fenSuffix = fen.substring(firstSpaceIndex, fen.length());

        String[] rows = boardPart.split("/");

        for (int rowNum = 0; rowNum < rows.length; rowNum++) {
            String row = rows[rowNum];
            //Replace numbers with the correct number of space characters
            String expandedRow = "";
            for (int colNum = 0; colNum < row.length(); colNum++) {
                char boardSpace = row.charAt(colNum);
                if (Character.isDigit(boardSpace)) {
                    int numEmpties = Character.getNumericValue(boardSpace);
                    for (int k = 0; k < numEmpties; k++) {
                        expandedRow += ' ';
                    }
                } else {
                    expandedRow += boardSpace;
                }
            }
            //Store each character of the expanded FEN string in the 2d board array
            for (int charPosition = 0; charPosition < expandedRow.length(); charPosition++) {
                board[rowNum][charPosition] = expandedRow.charAt(charPosition);
            }
        }
    }

    public String getFENForBoard() {
        StringBuilder result = new StringBuilder();
        for (int rowNum = 0; rowNum < board.length; rowNum++) {
            char[] row = board[rowNum];
            //keep track of the number of spaces so they can be replaced with the correct number
            int spacesFound = 0;
            for (char boardSpace : row) {
                if (boardSpace == ' ') {
                    spacesFound++;
                } else {
                    if (spacesFound > 0) {
                        result.append(spacesFound);
                        spacesFound = 0;
                    }
                    result.append(boardSpace);
                }
            }
            //Takes care of the empty row case
            if (spacesFound > 0) {
                result.append(spacesFound);
            }
            //Add / after each row but the last
            if (rowNum < board.length - 1) {
                result.append('/');
            }
        }
        return result.toString() + fenSuffix;
    }

    public void makeMove(String move) {
        if (move.length() != 4) {
            throw new IllegalArgumentException("must be length 4");
        }

        String initialCol = move.substring(0, 1);
        String initialRow = move.substring(1, 2);
        String finalCol = move.substring(2, 3);
        String finalRow = move.substring(3, 4);

        char piece = board[NOTATION_TO_BOARD_INDEX.get(initialRow)][NOTATION_TO_BOARD_INDEX.get(initialCol)];

        board[NOTATION_TO_BOARD_INDEX.get(finalRow)][NOTATION_TO_BOARD_INDEX.get(finalCol)] = piece;
        board[NOTATION_TO_BOARD_INDEX.get(initialRow)][NOTATION_TO_BOARD_INDEX.get(initialCol)] = ' ';
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("  ---------------------------------\n");
        for (int row = 0; row < board.length; row++) {
            sb.append(8 - row + " |");
            for (int col = 0; col < board[row].length; col++) {
                sb.append(" " + board[row][col] + " |");
            }
            if (row != board.length - 1) {
                sb.append("\n  |-------------------------------|\n");
            }
        }
        sb.append("\n  ---------------------------------\n");
        sb.append("    a   b   c   d   e   f   g   h  ");

        return sb.toString();
    }
}
