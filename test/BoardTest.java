import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class BoardTest {
    private static final String INITIAL_BOARD_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    private static final String INITIAL_BOARD =
            "  ---------------------------------\n" +
            "8 | r | n | b | q | k | b | n | r |\n" +
            "  |-------------------------------|\n" +
            "7 | p | p | p | p | p | p | p | p |\n" +
            "  |-------------------------------|\n" +
            "6 |   |   |   |   |   |   |   |   |\n" +
            "  |-------------------------------|\n" +
            "5 |   |   |   |   |   |   |   |   |\n" +
            "  |-------------------------------|\n" +
            "4 |   |   |   |   |   |   |   |   |\n" +
            "  |-------------------------------|\n" +
            "3 |   |   |   |   |   |   |   |   |\n" +
            "  |-------------------------------|\n" +
            "2 | P | P | P | P | P | P | P | P |\n" +
            "  |-------------------------------|\n" +
            "1 | R | N | B | Q | K | B | N | R |\n" +
            "  ---------------------------------\n" +
            "    a   b   c   d   e   f   g   h  ";

    private static final String EXAMPLE_FEN = "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2";
    private static final String EXAMPLE_BOARD =
            "  ---------------------------------\n" +
            "8 | r | n | b | q | k | b | n | r |\n" +
            "  |-------------------------------|\n" +
            "7 | p | p |   | p | p | p | p | p |\n" +
            "  |-------------------------------|\n" +
            "6 |   |   |   |   |   |   |   |   |\n" +
            "  |-------------------------------|\n" +
            "5 |   |   | p |   |   |   |   |   |\n" +
            "  |-------------------------------|\n" +
            "4 |   |   |   |   | P |   |   |   |\n" +
            "  |-------------------------------|\n" +
            "3 |   |   |   |   |   | N |   |   |\n" +
            "  |-------------------------------|\n" +
            "2 | P | P | P | P |   | P | P | P |\n" +
            "  |-------------------------------|\n" +
            "1 | R | N | B | Q | K | B |   | R |\n" +
            "  ---------------------------------\n" +
            "    a   b   c   d   e   f   g   h  ";

    private static final String BEFORE_MOVE1_FEN = "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2";
    private static final String MOVE1 = "a7a5";
    private static final String AFTER_MOVE1_FEN = "rnbqkbnr/1p1ppppp/8/p1p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2";

    private static final String INITIAL_BOARD_MOVE = "a2a3";
    private static final String AFTER_INITIAL_BOARD_MOVE_FEN = "rnbqkbnr/pppppppp/8/8/8/P7/1PPPPPPP/RNBQKBNR w KQkq - 0 1";

    private static final String BEFORE_MOVE2_FEN = "b7/8/8/8/8/8/8/7B b KQkq - 1 2";
    private static final String MOVE2 = "a8h1";
    private static final String AFTER_MOVE2_FEN = "8/8/8/8/8/8/8/7b b KQkq - 1 2";

    private Board board;

    @BeforeTest
    public void preTestSetup() {
        board = new Board();
    }

    @Test
    public void testPrintBoard() {
        board.setBoardState(EXAMPLE_FEN);
        assertEquals(board.toString(), EXAMPLE_BOARD);

        board.setBoardState(INITIAL_BOARD_FEN);
        assertEquals(board.toString(), INITIAL_BOARD);
    }

    @Test
    public void testGetFENForBoard() {
        board.setBoardState(EXAMPLE_FEN);
        assertEquals(board.getFENForBoard(), EXAMPLE_FEN);

        board.setBoardState(INITIAL_BOARD_FEN);
        assertEquals(board.getFENForBoard(), INITIAL_BOARD_FEN);
    }

    @Test
    public void testMakeMove() {
        board.setBoardState(INITIAL_BOARD_FEN);
        board.makeMove(INITIAL_BOARD_MOVE);
        assertEquals(board.getFENForBoard(), AFTER_INITIAL_BOARD_MOVE_FEN);

        board.setBoardState(BEFORE_MOVE1_FEN);
        board.makeMove(MOVE1);
        assertEquals(board.getFENForBoard(), AFTER_MOVE1_FEN);

        board.setBoardState(BEFORE_MOVE2_FEN);
        board.makeMove(MOVE2);
        assertEquals(board.getFENForBoard(), AFTER_MOVE2_FEN);
    }
}