import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SuggestedMoveClientTest {
    private static final String EXAMPLE_1_FEN = "8/4npk1/5p1p/1Q5P/1p4P1/4r3/7q/3K1R2 b - - 1 49";
    private static final String EXAMPLE_1_MOVE = "b4b3";

    private static final String EXAMPLE_2_FEN = "5r1k/6pp/4Qpb1/p7/8/6PP/P4PK1/3q4 b - - 4 37";
    private static final String EXAMPLE_2_MOVE = "a5a4";

    private static final String EXAMPLE_3_FEN = "8/8/2P5/4B3/1Q6/4K3/6P1/3k4 w - - 5 67";
    private static final String EXAMPLE_3_MOVE = "b4a3";

    private static final String EXAMPLE_4_FEN = "r2q1rk1/pp2ppbp/2p2np1/6B1/3PP1b1/Q1P2N2/P4PPP/3RKB1R b K - 0 13";
    private static final String EXAMPLE_4_MOVE = "a7a5";

    @Test
    public void testGetSuggestedMove() throws Exception {
        SuggestedMoveClient suggestedMoveClient = new SuggestedMoveClient();

        //Since these are calls to the actual api sleep for a second between them.
        assertEquals(suggestedMoveClient.getSuggestedMove(EXAMPLE_1_FEN), EXAMPLE_1_MOVE);
        Thread.sleep(1000);
        assertEquals(suggestedMoveClient.getSuggestedMove(EXAMPLE_2_FEN), EXAMPLE_2_MOVE);
        Thread.sleep(1000);
        assertEquals(suggestedMoveClient.getSuggestedMove(EXAMPLE_3_FEN), EXAMPLE_3_MOVE);
        Thread.sleep(1000);
        assertEquals(suggestedMoveClient.getSuggestedMove(EXAMPLE_4_FEN), EXAMPLE_4_MOVE);
    }

}