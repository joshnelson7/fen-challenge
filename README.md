# README #

There is a pre-built jar of this project in `out/artifacts/FENChallenge_jar`.


Use the following command to run the program `java -jar FENChallenge.jar --task1 | --task2 | --stretch "fen"`


Here are example commands for each task:


```
java -jar FENChallenge.jar --task1 "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2"

java -jar FENChallenge.jar --task2 "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2"

java -jar FENChallenge.jar --stretch "rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2"

```

You can also load the project into IntelliJ IDEA to build and run the tests.